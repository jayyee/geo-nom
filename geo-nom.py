from functools import reduce
import csv
import warnings
from helpers import notable_attribs
from helpers import address_attribs
from helpers import colors
from helpers import poi_type_text_attribs
from helpers import poi_type_text
import requests
from lxml import etree as ET


def geolookup(csv_file):
    with open(csv_file, 'r') as csvfile:
        with open('loc-' + csv_file, 'w') as csvfile_loc:
            rows = csv.DictReader(csvfile)
            writer = csv.DictWriter(csvfile_loc,
                                    fieldnames=rows.fieldnames)
            writer.writeheader()

            osm_data = []

            for row in rows:
                print('working on: ' + row.get('Name'))
                name = row.get('Name')

                if not row.get('Personal'):
                    name = ''

                osm_params = {'location': {'lat': row.get('Lat'),
                                           'lon': row.get('Lon'),},
                              'name': name,
                              'type': row.get('POI_Type'),
                              'id': row.get('POI_ID'),
                              'orig_query': True, }

                row_data = osm_query(osm_params)
                if row_data:
                    osm_data.append(row_data)
                    writer.writerow({'Name': row.get('Name'),
                                     'POI_Type': row.get('POI_Type'),
                                     'POI_ID': row.get('POI_ID'),
                                     'Lat': row_data.get('location').get('lat'),
                                     'Lon': row_data.get('location').get('lon'),
                                     'Notes': row.get('Notes'),
                                     'Personal': row.get('Personal'),})
                else:
                    print(osm_params.get('type')
                          + osm_params.get('id'), 'is not in OSM.')

    return osm_data


OSM_TYPES = {'R': 'relation',
             'W': 'way',
             'N': 'node',}


def osm_request(poi_type, poi_id):
    get_spec = OSM_TYPES.get(poi_type) + '/' + str(poi_id)
    req = requests.get('https://www.openstreetmap.org/api/0.6/' +
                       get_spec)
    if not req.content:
        return False
    return ET.fromstring(req.content)

def node_location(tree):
    return {
        'lat': float(tree[0].attrib.get('lat')),
        'lon': float(tree[0].attrib.get('lon')),
    }


def child_location(children, child_type):
    return reduce(lambda lat, lon:
                  {'lat': lat / len(children),
                   'lon': lon / len(children),},
                  reduce(lambda coord1, coord2:
                         {'lat': coord1.get('lat') + coord2.get('lat'),
                          'lon': coord1.get('lon') + coord2.get('lon'),},
                         list(map(lambda node:
                                  osm_query({'type': child_type,
                                             'id': node.attrib.get('ref'),
                                             'orig_query': False,}),
                                  children))).values())


def osm_query(osm_params):
    tree = osm_request(osm_params.get('type'),
                       osm_params.get('id'))
    if tree is False:
        return False

    if osm_params.get('type') == 'N':
        location = node_location(tree)
    elif (osm_params.get('orig_query')
          and osm_params.get('location').get('lat')
          and osm_params.get('location').get('lon')
          and osm_params.get('type') != 'N'):
        location = {'lat': float(osm_params.get('location').get('lat')),
                    'lon': float(osm_params.get('location').get('lon')),}
    elif osm_params.get('type') == 'W':
        nodes = list(filter(lambda x: x.tag == 'nd', tree[0]))
        location = child_location(nodes, 'N')
    elif osm_params.get('type') == 'R':
        ways = list(filter(lambda x: x.tag == 'member', tree[0]))
        location = child_location(ways, 'W')

    if osm_params.get('orig_query'):
        return {'location': location,
                'tags': get_notable_attribs(tree[0]),
                'name': osm_params.get('name'),}
    return location


def create_address(addr_attribs):
    if addr_attribs.get('addr:unit') is None:
        unit = ''
    else:
        unit = ' ' + addr_attribs.get('addr:unit')
    first_line = (addr_attribs.get('addr:housenumber')
                  + ' '
                  + addr_attribs.get('addr:street')
                  + unit)
    second_line = (addr_attribs.get('addr:city')
                   + ', '
                   + addr_attribs.get('addr:state')
                   + ' '
                   + addr_attribs.get('addr:postcode'))
    return first_line + '\n' + second_line


def sufficient_address(addr_attribs):
    return (addr_attribs.get('addr:housenumber') or
            addr_attribs.get('addr:street') or
            addr_attribs.get('addr:city') or
            addr_attribs.get('addr:state') or
            addr_attribs.get('addr:postecode')) is None


def get_notable_attribs(attribs):
    notable_osm_attribs = {}
    osm_address_attribs = {}
    for a in attribs:
        if a.attrib.get('k') in notable_attribs:
            notable_osm_attribs.update({a.attrib.get('k'): a.attrib.get('v')})
        elif a.attrib.get('k') in address_attribs:
            osm_address_attribs.update({a.attrib.get('k'): a.attrib.get('v')})
    if set(osm_address_attribs.keys()) >= (address_attribs - set(('addr:unit',))):
        return {'address': osm_address_attribs,
                'notable_osm_attribs': notable_osm_attribs,}
    return {'notable_osm_attribs': notable_osm_attribs,}

def add_to_gpx(poi, gpx):
    wpt = ET.Element('wpt',
                     lat=str(poi.get('location').get('lat')),
                     lon=str(poi.get('location').get('lon')),)
    name = ET.Element('name')

    if poi.get('name'):
        name.text = poi.get('name')
    else:
        name.text = poi.get('tags').get('notable_osm_attribs').get('name', '')

    poi_type = ET.Element('type')
    poi_tt = poi_type_text(poi)
    poi_type.text = poi_tt.capitalize()

    extensions = ET.Element('extensions')
    color = ET.Element('color')
    color_name = poi_type_text_attribs.get(poi_tt).get('color')
    color.text = colors.get(color_name)

    desc = ET.Element('desc')

    address_fields = poi.get('tags').get('address')
    if (address_fields is not None and sufficient_address(address_fields)):
        address = ('Address:\n'
                   + create_address(address_fields))
    else:
        address = ''

    desc.text = ET.CDATA(string_tags(poi.get('tags').get('notable_osm_attribs'))
                         + address)

    extensions.append(color)
    wpt.append(name)
    wpt.append(desc)
    wpt.append(poi_type)
    wpt.append(extensions)
    gpx.append(wpt)
    return gpx


def string_tags(tags):
    desc_text = ''
    for k,v in tags.items():
        if k != 'name':
            desc_text += (k.capitalize() + ': ' + v + ' &#xD;\n\n')
    return desc_text


def write_gpx():
    gpx_file = open('boiler.gpx', 'r').read()
    gpx = ET.fromstring(gpx_file)
    for poi in geolookup('places.csv'):
        gpx = add_to_gpx(poi, gpx)
    gpx_output = ET.ElementTree(gpx)
    gpx_output.write('import.gpx',
                     pretty_print=True,
                     encoding='UTF-8',
                     method='html',
                     xml_declaration=False,)

write_gpx()
